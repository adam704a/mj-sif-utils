package rcd.rti.org;

import com.mongodb.*;
import com.mongodb.client.FindIterable;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * Hello world!
 *
 */
public class App 
{

    private static String userName = "datascientist";
    private static String database = "mj_sample";
    private static String password = "pass";

    public static void main( String[] args )
    {
        MongoCredential credential = MongoCredential.createCredential(userName, database, password.toCharArray());

        MongoClient mongoClient = new MongoClient(new ServerAddress("ec2-52-1-24-47.compute-1.amazonaws.com"), Arrays.asList(credential));


        MongoDatabase db = mongoClient.getDatabase("mj_sample");
        MongoCollection collection = db.getCollection("mj_sample");


        Calendar startCalendar = new GregorianCalendar(2014,11,1);
        java.util.Date startdate = startCalendar.getTime();

        Calendar endCalendar = new GregorianCalendar(2014,11,15);
        java.util.Date endate = endCalendar.getTime();

        BasicDBObject dateQueryObj = new BasicDBObject("Date",  new BasicDBObject("$gt", startdate).append("$lte", endate));

        FindIterable<Document> iterable = collection.find(dateQueryObj);

        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document);
            }
        });

        //System.out.println("Connected: " + collection.count());

    }
}
